import { Component, OnInit } from '@angular/core';
import {ToDoListService} from "../../services/to-do-list.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../models/User";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  itemForm: FormGroup;
  users: User[] = [];
  titleControl: FormControl;
  contentControl: FormControl;
  statusControl: FormControl;
  positionControl: FormControl;
  userControl: FormControl;

  constructor(private toDoListService: ToDoListService, private userService: UserService, private router: Router, formBuilder: FormBuilder) {
    this.titleControl = formBuilder.control('', [Validators.required]);
    this.contentControl = formBuilder.control('', [Validators.required]);
    this.statusControl = formBuilder.control('', [Validators.required]);
    this.positionControl = formBuilder.control('', [Validators.required]);
    this.userControl = formBuilder.control('', [Validators.required]);

    this.itemForm = formBuilder.group({
      title: this.titleControl,
      content: this.contentControl,
      status: this.statusControl,
      position: this.positionControl,
      user: this.userControl
    });
  }

  ngOnInit() {
    this.getAllUsers();
  }

  onSubmit(){
    this.toDoListService.addItem(this.itemForm.value).subscribe(data => {
      console.log(data);
    });
  }

  getAllUsers(){
    this.userService.getUsers().subscribe((data) => {
      this.users = data;
    });
  }

}
