import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ToDoListComponent} from "./components/to-do-list/to-do-list.component";
import {ItemComponent} from "./components/item/item.component";

const routes: Routes = [
  {path: '', component: ToDoListComponent},
  {path: '**', component: ItemComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
