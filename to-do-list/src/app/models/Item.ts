export class Item {
  position: number;
  title: string;
  status: string;
  content: string;
  user: string;
}
